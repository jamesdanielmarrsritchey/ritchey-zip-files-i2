<?php
#Name:Ritchey Zip Files i2 v1
#Description:Create a zip archive of specified files. Returns "TRUE' on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. 
#Arguments:'files' (required) is an array of files to archive. 'destination' (required) is a path to where to write the zip, including the filename. 'stripe' (optional) is a number of characters to remove from the front of each file path. By default, the full path of each file is used. Stripping can be useful to exclude certain parents. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):files:array:required,destination:path:required,stripe:number:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_zip_files_i2_v1') === FALSE){
function ritchey_zip_files_i2_v1($files, $destination, $stripe = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@is_array($files) === FALSE){
		$errors[] = "files";
	}
	$check = FALSE;
	foreach ($files as $file) {
		if (@is_file($file) === FALSE) {
			$check = TRUE;
		}
	}
	unset($file);
	if ($check === TRUE){
		$errors[] = "files";
	}
	if (@is_dir(@dirname($destination)) === FALSE){
		$errors[] = "destination";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task (Add files to zip)
	if (@empty($errors) === TRUE){
		###Create zip archive
		$archive = new ZipArchive;
		$archive->open("$destination", ZIPARCHIVE::OVERWRITE | ZIPARCHIVE::CREATE);
		if ($archive == TRUE){
			foreach($files as $file){
				####Adjust file path in zip
				$file_path_in_zip = $file;
				if (@is_int($stripe) === TRUE){
					$stripe_position = $stripe;
					$file_path_in_zip = @substr($file_path_in_zip, $stripe_position);
				}
				$archive->addFile("$file", "$file_path_in_zip");
			}
			unset($file);
			$archive->close();
		} else {
			$errors[] = "task (creating zip)";
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_zip_files_i2_v1_format_error') === FALSE){
				function ritchey_zip_files_i2_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_zip_files_i2_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>